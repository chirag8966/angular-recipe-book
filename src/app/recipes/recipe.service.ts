import {Injectable, EventEmitter } from '@angular/core';
import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';


@Injectable()
export class RecipeService {
  recipeSelected = new EventEmitter<Recipe>();

  private recipes: Recipe [] = [
    new Recipe ('Tasty food' ,
     'photo looks delicious' ,
      'https://images.unsplash.com/photo-1559160582-eab6966b680f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
      [
        new Ingredient(' Meat ', 1 ),
        new Ingredient(' French Fries ', 20 ),

      ]),

    new Recipe ('just some food' ,
    'another delicious food' ,
     'https://50campfires.com/wp-content/uploads/2015/10/21-smore-cover.jpg',
     [
      new Ingredient(' Meat ', 1),
      new Ingredient(' Buns ', 2),
     ])
    ];

    constructor(private slService: ShoppingListService) {}

    getRecipes() {
      return this.recipes.slice();
    }

    getRecipe(index: number) {
      return this.recipes[index];
    }

    addIngredientsToShoppingList(ingredients: Ingredient[]) {
      this.slService.addIngredients(ingredients);
    }
}
